const express = require('express')
const server = express()
const request = require('request')

__dirname = 'C:/Users/chori/Documents/Ivory Files/20180405 - GWIC/cutup/actual/'

server.get('/', function (req, res) {
    res.sendFile(__dirname + 'index.html')
})
server.get('/about', function (req, res) {
    res.sendFile(__dirname + 'about.html')
})

server.use('/css', express.static(__dirname + '/css'))
server.use('/linearicons', express.static(__dirname + '/linearicons'))
server.use('/bower_components', express.static(__dirname + '/bower_components'))
server.use('/js', express.static(__dirname + '/js'))
server.use('/images', express.static(__dirname + '/images'))
server.listen(3000)