var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');

gulp.task('sass', function () {
    return gulp.src('styles/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}, {errLogToConsole: true}))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(prefix("last 2 versions"))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css'))
        .pipe(reload({stream:true}));
});

gulp.task('browser-sync', ['nodemon'], function() {
    browserSync.init(null, {
        proxy: "http://localhost:3000",
        port: 8000
    });
});

gulp.task('movepearicons', function(){
    // the base option sets the relative root for the set of files,
    // preserving the folder structure
    gulp.src('./pearicons/**/*.*', { base: './' })
        .pipe(gulp.dest('css'));
});

gulp.task('default', ['sass', 'browser-sync'], function () {
    gulp.watch("scss/**/*", ['sass']);
    gulp.watch("styles/**/*", ['sass']);
    gulp.watch(["*.html"], reload);
});

gulp.task('nodemon', function (cb) {
    var called = false;
    return nodemon({script: 'scripts/server.js'}).on('start', function () {
        if (!called) {
            called = true;
            cb();
        }
    });
});